package com.future.jayn.crimeintents;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import android.content.Context;

public class CrimeLab {
	private static CrimeLab crimeLab;
	private Context appContext;
	
	private List<Crime> crimes;
	
	private CrimeLab(Context appContext) {
		this.appContext = appContext;
		init();
	}
	
	private void init() {
		crimes = new ArrayList<Crime>();
		
		for(int i = 0; i < 100; i++) {
			Crime crime = new Crime();
			crime.setTitle("Crime #" + i);
			crime.setSolved(i % 2 == 0);
			
			crimes.add(crime);
		}
	}
	
	public static CrimeLab getInstance(Context context) {
		if(crimeLab == null) {
			crimeLab = new CrimeLab(context.getApplicationContext());
		}
		
		return crimeLab;
	}
	
	public List<Crime> getCrimes() {
		return crimes;
	}
	
	public Crime getCrime(UUID id) {
		for(Crime crime : crimes) {
			if(crime.getId().equals(id)) {
				return crime;
			}
		}
		
		return null;
	}
}
